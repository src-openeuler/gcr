%ifarch %{valgrind_arches}
%global         has_valgrind 1
%endif

Name:           gcr
Version:        3.41.2
Release:        2
Summary:        A library for bits of crypto UI and parsing
License:        LGPLv2+
URL:            https://wiki.gnome.org/Projects/CryptoGlue
Source0:        https://download-fallback.gnome.org/sources/%{name}/3.41/%{name}-%{version}.tar.xz

Patch9000:      remove-sensitive-info.patch

BuildRequires:  pkgconfig(gio-unix-2.0) pkgconfig(gobject-introspection-1.0) pkgconfig(gtk+-3.0) pkgconfig(p11-kit-1)
BuildRequires:  chrpath docbook-style-xsl libgcrypt-devel desktop-file-utils intltool vala gnupg2 libxslt meson
BuildRequires:  gi-docgen gettext cmake pkgconfig(libsecret-1) pkgconfig(libsystemd)
%if 0%{?has_valgrind}
BuildRequires:  valgrind-devel
%endif
BuildRequires:  /usr/bin/gpg2
BuildRequires:  openssh-clients
BuildRequires:  openssh-clients
BuildRequires:  libxslt

Requires: %{name}-base%{?_isa} = %{version}-%{release}
Requires: openssh-clients
Requires: openssh-clients

%description
gcr is a library for displaying certificates, and crypto UI, accessing key stores. It also provides a viewer for
crypto files on the GNOME desktop.gck is a library for accessing PKCS#11 modules like smart cards.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}

%description    devel
The gcr-devel package includes the header files for the gcr library.

%package base
Summary: Library files for gcr
Conflicts: %{name} < 3.28.1-3

%description base
The gcr-base package includes the gcr-base library.

%prep
%autosetup -n %{name}-%{version} -p1
# Use system valgrind headers instead
%if 0%{?has_valgrind}
rm -rf build/valgrind/
%endif

%build
%meson
%meson_build

%install
%meson_install
%find_lang %{name}

%check
desktop-file-validate $RPM_BUILD_ROOT%{_datadir}/applications/gcr-viewer.desktop

%post

%preun

%postun

%files -f %{name}.lang
%defattr(-,root,root)
%doc NEWS README.md
%license COPYING
%{_bindir}/gcr-viewer
%{_datadir}/applications/gcr-viewer.desktop
%dir %{_datadir}/GConf
%dir %{_datadir}/GConf/gsettings
%{_datadir}/GConf/gsettings/org.gnome.crypto.pgp.convert
%{_datadir}/GConf/gsettings/org.gnome.crypto.pgp_keyservers.convert
%{_datadir}/glib-2.0/schemas/org.gnome.crypto.pgp.gschema.xml
%{_libdir}/girepository-1.0
%{_libdir}/libgcr-ui-3.so.1*
%{_datadir}/icons/hicolor/*/apps/*
%{_datadir}/mime/packages/gcr-crypto-types.xml
%{_libexecdir}/gcr-prompter
%exclude %{_libexecdir}/gcr-ssh-agent
%{_libexecdir}/gcr-ssh-askpass
%{_datadir}/dbus-1/services/org.gnome.keyring.PrivatePrompter.service
%{_datadir}/dbus-1/services/org.gnome.keyring.SystemPrompter.service
%{_datadir}/applications/gcr-prompter.desktop
%exclude %{_userunitdir}/gcr-ssh-agent.service
%exclude %{_userunitdir}/gcr-ssh-agent.socket

%files devel
%{_includedir}/gck-1
%{_includedir}/gcr-3
%{_libdir}/libgck-1.so
%{_libdir}/libgcr-base-3.so
%{_libdir}/libgcr-ui-3.so
%{_libdir}/pkgconfig/gck-1.pc
%{_libdir}/pkgconfig/gcr-3.pc
%{_libdir}/pkgconfig/gcr-base-3.pc
%{_libdir}/pkgconfig/gcr-ui-3.pc
%{_datadir}/gir-1.0
%{_datadir}/doc
%{_datadir}/vala/

%files base
%{_libdir}/libgck-1.so.0*
%{_libdir}/libgcr-base-3.so.1*

%changelog
* Wed Nov 06 2024 zhangpan <zhangpan103@h-partners.com> - 3.41.2-2
- fix changelog error

* Mon Feb 05 2024 zhangpan <zhangpan103@h-partners.com> - 3.41.2-1
- update to 3.41.2

* Thu Mar 02 2023 wenlong ding <wenlong.ding@turbolinux.com.cn> - 3.41.1-3
- Exclude confics file which already in gcr4

* Mon Feb 13 2023 zhangpan <zhangpan103@h-partners.com> - 3.41.1-2
- remove sensitive info

* Fri Nov 11 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 3.41.1-1
- update to gcr-3.41.1

* Mon Jun 20 2022 wuchaochao <wuchaochao4@h-partners.com> - 3.41.0-2
- Fix unknown kw argument in gnome.generate_gir

* Mon Dec 6 2021 hanhui <hanhui15@huawei.com> - 3.41.0-1
- update to gcr-3.41.0

* Fri Jan 29 2021 yanglu <yanglu60@huawei.com> - 3.38.1-1
- update to 3.38.1

* Mon Jul 20 2020 wangye <wangye70@huawei.com> - 3.36.0-1
- update to 3.36.0

* Sat Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.34.0-1
- update to 3.34.0

* Wed Sep 11 2019 openEuler jimmy<dukaitian@huawei.com>  - 3.28.0-3
- Package init jimmy
